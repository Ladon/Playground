import QtQuick 2.0

Item {
    width: 128
    height: 128

    Rectangle {
        id: rectangle
        color: "#0ff0f0"
        radius: 9
        anchors.fill: parent
    }

    Text {
        id: text1
        text: qsTr("Button")
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 12
    }

}
