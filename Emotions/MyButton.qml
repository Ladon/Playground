import QtQuick 2.0

Item {
    Rectangle {
        id: rectangle
        x: 128
        y: 73
        width: 200
        height: 200
        color: "#ffffff"

        Text {
            id: text1
            x: 67
            y: 50
            width: 82
            height: 29
            text: qsTr("MyButton")
            font.pixelSize: 12
        }
    }

}
