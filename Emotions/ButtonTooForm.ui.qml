import QtQuick 2.4

Item {
    width: 400
    height: 400

    Rectangle {
        id: rectangle
        color: "#090807"
        radius: 43
        anchors.fill: parent
    }

    Text {
        id: text1
        text: qsTr("ButtonToo")
        anchors.fill: parent
        font.pixelSize: 12
    }
}
