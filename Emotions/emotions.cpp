#include "emotions.h"
#include "ui_emotions.h"

Emotions::Emotions(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Emotions),
    emotions_Total(0)
{
    statusBarLabel = new QLabel;
    for(uchar i = 0; i < 48; i++)
        emotion[i] = 0;
    ui->setupUi(this);
    ui->statusBar->addWidget(statusBarLabel);
}

Emotions::~Emotions()
{
    if (file_csv.isOpen())
        file_csv.close();
    delete ui;
}

//    ui->pushButton_A->setText(QString("pressed %1 times").arg(emotion_A));

void Emotions::on_pushButton_Serenity_pressed()
{
    pushButton_pressed(0);
}

void Emotions::on_pushButton_Acceptance_pressed()
{
    pushButton_pressed(1);
}

void Emotions::on_pushButton_Apprehension_pressed()
{
    pushButton_pressed(2);
}

void Emotions::on_pushButton_Distraction_pressed()
{
    pushButton_pressed(3);
}

void Emotions::on_pushButton_Pensiveness_pressed()
{
    pushButton_pressed(4);
}

void Emotions::on_pushButton_Boredom_pressed()
{
    pushButton_pressed(5);
}

void Emotions::on_pushButton_Annoyance_pressed()
{
    pushButton_pressed(6);
}

void Emotions::on_pushButton_Interest_pressed()
{
    pushButton_pressed(7);
}

void Emotions::on_pushButton_Joy_pressed()
{
    pushButton_pressed(8);
}

void Emotions::on_pushButton_Trust_pressed()
{
    pushButton_pressed(9);
}

void Emotions::on_pushButton_Fear_pressed()
{
    pushButton_pressed(10);
}

void Emotions::on_pushButton_Surprise_pressed()
{
    pushButton_pressed(11);
}

void Emotions::on_pushButton_Sadness_pressed()
{
    pushButton_pressed(12);
}

void Emotions::on_pushButton_Disgust_pressed()
{
    pushButton_pressed(13);
}

void Emotions::on_pushButton_Anger_pressed()
{
    pushButton_pressed(14);
}

void Emotions::on_pushButton_Anticipation_pressed()
{
    pushButton_pressed(15);
}

void Emotions::on_pushButton_Ecstasy_pressed()
{
    pushButton_pressed(16);
}

void Emotions::on_pushButton_Admiration_pressed()
{
    pushButton_pressed(17);
}

void Emotions::on_pushButton_Terror_pressed()
{
    pushButton_pressed(18);
}

void Emotions::on_pushButton_Amazement_pressed()
{
    pushButton_pressed(19);
}

void Emotions::on_pushButton_Grief_pressed()
{
    pushButton_pressed(20);
}

void Emotions::on_pushButton_Loathing_pressed()
{
    pushButton_pressed(21);
}

void Emotions::on_pushButton_Rage_pressed()
{
    pushButton_pressed(22);
}

void Emotions::on_pushButton_Vigilance_pressed()
{
    pushButton_pressed(23);
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                           *
 *            D y a d s   ( c o m b i n a t i o n s )        *
 *                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

void Emotions::on_pushButton_Aggressiveness_pressed()
{
    pushButton_pressed(24);
    pushButton_pressed(14);
    pushButton_pressed(15); // anticipation //
}

void Emotions::on_pushButton_Optimism_pressed()
{
    pushButton_pressed(25);
    pushButton_pressed(8);
    pushButton_pressed(15); // anticipation //
}

void Emotions::on_pushButton_Love_pressed()
{
    pushButton_pressed(26);
    pushButton_pressed(8); // joy //
    pushButton_pressed(9); // trust //
}

void Emotions::on_pushButton_Submission_pressed()
{
    pushButton_pressed(27);
    pushButton_pressed(9); // trust //
    pushButton_pressed(10); // fear //
}

void Emotions::on_pushButton_Awe_pressed()
{
    pushButton_pressed(28);
    pushButton_pressed(10); // fear //
    pushButton_pressed(11); // surprise //
}

void Emotions::on_pushButton_Disapproval_pressed()
{
    pushButton_pressed(29);
    pushButton_pressed(11); // surprise //
    pushButton_pressed(12); // sadness //
}

void Emotions::on_pushButton_Remorse_pressed()
{
    pushButton_pressed(30);
    pushButton_pressed(12); // sadness //
    pushButton_pressed(13); // disgust //
}

void Emotions::on_pushButton_Contempt_pressed()
{
    pushButton_pressed(31);
    pushButton_pressed(13); // disgust //
    pushButton_pressed(14); // anger //
}

void Emotions::on_pushButton_Envy_pressed()
{
    pushButton_pressed(32);
    pushButton_pressed(12); // sadness //
    pushButton_pressed(14); // anger //
}

void Emotions::on_pushButton_Cynicism_pressed()
{
    pushButton_pressed(33);
    pushButton_pressed(13); // disgust //
    pushButton_pressed(15); // anticipation //
}

void Emotions::on_pushButton_Pride_pressed()
{
    pushButton_pressed(34);
    pushButton_pressed(14); // anger //
    pushButton_pressed(8); // joy //
}

void Emotions::on_pushButton_Hope_pressed()
{
    pushButton_pressed(35);
    pushButton_pressed(15); // anticipation //
    pushButton_pressed(9); // trust //
}

void Emotions::on_pushButton_Guilt_pressed()
{
    pushButton_pressed(36);
    pushButton_pressed(8); // joy //
    pushButton_pressed(10); // fear //
}

void Emotions::on_pushButton_Curiosity_pressed()
{
    pushButton_pressed(37);
    pushButton_pressed(9); // trust //
    pushButton_pressed(11); // surprise //
}

void Emotions::on_pushButton_Despair_pressed()
{
    pushButton_pressed(38);
    pushButton_pressed(10); // fear //
    pushButton_pressed(12); // sadness //
}

void Emotions::on_pushButton_Unbelief_pressed()
{
    pushButton_pressed(39);
    pushButton_pressed(11); // surprise //
    pushButton_pressed(13); // disgust //
}

void Emotions::on_pushButton_Sentimentality_pressed()
{
    pushButton_pressed(40);
    pushButton_pressed(9); // trust //
    pushButton_pressed(12); // sadness //
}

void Emotions::on_pushButton_Shame_pressed()
{
    pushButton_pressed(41);
    pushButton_pressed(10); // fear //
    pushButton_pressed(13); // disgust //
}

void Emotions::on_pushButton_Outrage_pressed()
{
    pushButton_pressed(42);
    pushButton_pressed(11); // surprise //
    pushButton_pressed(14); // anger //
}

void Emotions::on_pushButton_Pessimism_pressed()
{
    pushButton_pressed(43);
    pushButton_pressed(12); // sadness //
    pushButton_pressed(15); // anticipation //
}

void Emotions::on_pushButton_Morbidness_pressed()
{
    pushButton_pressed(44);
    pushButton_pressed(13); // disgust //
    pushButton_pressed(8); // joy //
}

void Emotions::on_pushButton_Dominance_pressed()
{
    pushButton_pressed(45);
    pushButton_pressed(14); // anger //
    pushButton_pressed(9); // trust //
}

void Emotions::on_pushButton_Anxiety_pressed()
{
    pushButton_pressed(46);
    pushButton_pressed(15); // anticipation //
    pushButton_pressed(10); // fear //
}

void Emotions::on_pushButton_Delight_pressed()
{
    pushButton_pressed(47);
    pushButton_pressed(8); // joy //
    pushButton_pressed(11); // surprise //
}

void Emotions::pushButton_pressed(const uchar& i)
{
    emotions_Total++;
    emotion[i]++;
    Emotions::append_emotion(i);
    Emotions::update_ratios();
}

short Emotions::emotion_max(void) const
{
    short max = 0;
    for (uchar i = 0; i < 24; i++) { // the rest are combinations with max_comb <= max(elem_1,elem_2) //
        if (emotion[i] > max)
            max = emotion[i];
    }
    return max;
}

void Emotions::emotions_clear(void)
{
    for (uchar i = 0; i < 48; i++)
        emotion[i] = 0;
    emotions_Total = 0;
}

void Emotions::append_emotion(const ushort& emotion_id)
{
    if (!file_csv.isOpen()) {
        // open file code adapted from Qt getting started notepad example //
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QString());

        if (!fileName.isEmpty()) {
            file_csv.setFileName(fileName);
            if (!file_csv.open(QIODevice::Append)) {
                QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
                return;
            }
            update_status_bar(fileName);
        }
    }
    // file_csv is now open //
    QTextStream textStream(&file_csv);
    // FIXME> change the following to Qt::ISODateWithMs on Qt 5.8.0 //
    textStream << emotion_id << ',' << QDateTime::currentDateTimeUtc().toString(Qt::ISODate) << endl;
}

void Emotions::update_ratios(void)
{
    short max = emotion_max();
    ui->progressBar_Serenity->setMaximum(max);
    ui->progressBar_Serenity->setValue(emotion[0]);
    ui->progressBar_Acceptance->setMaximum(max);
    ui->progressBar_Acceptance->setValue(emotion[1]);
    ui->progressBar_Apprehension->setMaximum(max);
    ui->progressBar_Apprehension->setValue(emotion[2]);
    ui->progressBar_Distraction->setMaximum(max);
    ui->progressBar_Distraction->setValue(emotion[3]);
    ui->progressBar_Pensiveness->setMaximum(max);
    ui->progressBar_Pensiveness->setValue(emotion[4]);
    ui->progressBar_Boredom->setMaximum(max);
    ui->progressBar_Boredom->setValue(emotion[5]);
    ui->progressBar_Annoyance->setMaximum(max);
    ui->progressBar_Annoyance->setValue(emotion[6]);
    ui->progressBar_Interest->setMaximum(max);
    ui->progressBar_Interest->setValue(emotion[7]);
    ui->progressBar_Joy->setMaximum(max);
    ui->progressBar_Joy->setValue(emotion[8]);
    ui->progressBar_Trust->setMaximum(max);
    ui->progressBar_Trust->setValue(emotion[9]);
    ui->progressBar_Fear->setMaximum(max);
    ui->progressBar_Fear->setValue(emotion[10]);
    ui->progressBar_Surprise->setMaximum(max);
    ui->progressBar_Surprise->setValue(emotion[11]);
    ui->progressBar_Sadness->setMaximum(max);
    ui->progressBar_Sadness->setValue(emotion[12]);
    ui->progressBar_Disgust->setMaximum(max);
    ui->progressBar_Disgust->setValue(emotion[13]);
    ui->progressBar_Anger->setMaximum(max);
    ui->progressBar_Anger->setValue(emotion[14]);
    ui->progressBar_Anticipation->setMaximum(max);
    ui->progressBar_Anticipation->setValue(emotion[15]);
    ui->progressBar_Ecstasy->setMaximum(max);
    ui->progressBar_Ecstasy->setValue(emotion[16]);
    ui->progressBar_Admiration->setMaximum(max);
    ui->progressBar_Admiration->setValue(emotion[17]);
    ui->progressBar_Terror->setMaximum(max);
    ui->progressBar_Terror->setValue(emotion[18]);
    ui->progressBar_Amazement->setMaximum(max);
    ui->progressBar_Amazement->setValue(emotion[19]);
    ui->progressBar_Grief->setMaximum(max);
    ui->progressBar_Grief->setValue(emotion[20]);
    ui->progressBar_Loathing->setMaximum(max);
    ui->progressBar_Loathing->setValue(emotion[21]);
    ui->progressBar_Rage->setMaximum(max);
    ui->progressBar_Rage->setValue(emotion[22]);
    ui->progressBar_Vigilance->setMaximum(max);
    ui->progressBar_Vigilance->setValue(emotion[23]);
    ui->progressBar_Aggressiveness->setMaximum(max);
    ui->progressBar_Aggressiveness->setValue(emotion[24]);
    ui->progressBar_Optimism->setMaximum(max);
    ui->progressBar_Optimism->setValue(emotion[25]);
    ui->progressBar_Love->setMaximum(max);
    ui->progressBar_Love->setValue(emotion[26]);
    ui->progressBar_Submission->setMaximum(max);
    ui->progressBar_Submission->setValue(emotion[27]);
    ui->progressBar_Awe->setMaximum(max);
    ui->progressBar_Awe->setValue(emotion[28]);
    ui->progressBar_Disapproval->setMaximum(max);
    ui->progressBar_Disapproval->setValue(emotion[29]);
    ui->progressBar_Remorse->setMaximum(max);
    ui->progressBar_Remorse->setValue(emotion[30]);
    ui->progressBar_Contempt->setMaximum(max);
    ui->progressBar_Contempt->setValue(emotion[31]);
    ui->progressBar_Envy->setMaximum(max);
    ui->progressBar_Envy->setValue(emotion[32]);
    ui->progressBar_Cynicism->setMaximum(max);
    ui->progressBar_Cynicism->setValue(emotion[33]);
    ui->progressBar_Pride->setMaximum(max);
    ui->progressBar_Pride->setValue(emotion[34]);
    ui->progressBar_Hope->setMaximum(max);
    ui->progressBar_Hope->setValue(emotion[35]);
    ui->progressBar_Guilt->setMaximum(max);
    ui->progressBar_Guilt->setValue(emotion[36]);
    ui->progressBar_Curiosity->setMaximum(max);
    ui->progressBar_Curiosity->setValue(emotion[37]);
    ui->progressBar_Despair->setMaximum(max);
    ui->progressBar_Despair->setValue(emotion[38]);
    ui->progressBar_Unbelief->setMaximum(max);
    ui->progressBar_Unbelief->setValue(emotion[39]);
    ui->progressBar_Sentimentality->setMaximum(max);
    ui->progressBar_Sentimentality->setValue(emotion[40]);
    ui->progressBar_Shame->setMaximum(max);
    ui->progressBar_Shame->setValue(emotion[41]);
    ui->progressBar_Outrage->setMaximum(max);
    ui->progressBar_Outrage->setValue(emotion[42]);
    ui->progressBar_Pessimism->setMaximum(max);
    ui->progressBar_Pessimism->setValue(emotion[43]);
    ui->progressBar_Morbidness->setMaximum(max);
    ui->progressBar_Morbidness->setValue(emotion[44]);
    ui->progressBar_Dominance->setMaximum(max);
    ui->progressBar_Dominance->setValue(emotion[45]);
    ui->progressBar_Anxiety->setMaximum(max);
    ui->progressBar_Anxiety->setValue(emotion[46]);
    ui->progressBar_Delight->setMaximum(max);
    ui->progressBar_Delight->setValue(emotion[47]);
}

void Emotions::update_status_bar(const QString& fileName)
{
    statusBarLabel->setText("Using file " + fileName);
//    ui->statusBar->showMessage("Using file " + fileName);
}

void Emotions::on_actionClear_triggered()
{
    Emotions::emotions_clear();
    ui->progressBar_Serenity->setMaximum(100);
    ui->progressBar_Serenity->setValue(0);
    ui->progressBar_Acceptance->setMaximum(100);
    ui->progressBar_Acceptance->setValue(0);
    ui->progressBar_Apprehension->setMaximum(100);
    ui->progressBar_Apprehension->setValue(0);
    ui->progressBar_Distraction->setMaximum(100);
    ui->progressBar_Distraction->setValue(0);
    ui->progressBar_Pensiveness->setMaximum(100);
    ui->progressBar_Pensiveness->setValue(0);
    ui->progressBar_Boredom->setMaximum(100);
    ui->progressBar_Boredom->setValue(0);
    ui->progressBar_Annoyance->setMaximum(100);
    ui->progressBar_Annoyance->setValue(0);
    ui->progressBar_Interest->setMaximum(100);
    ui->progressBar_Interest->setValue(0);
    ui->progressBar_Joy->setMaximum(100);
    ui->progressBar_Joy->setValue(0);
    ui->progressBar_Trust->setMaximum(100);
    ui->progressBar_Trust->setValue(0);
    ui->progressBar_Fear->setMaximum(100);
    ui->progressBar_Fear->setValue(0);
    ui->progressBar_Surprise->setMaximum(100);
    ui->progressBar_Surprise->setValue(0);
    ui->progressBar_Sadness->setMaximum(100);
    ui->progressBar_Sadness->setValue(0);
    ui->progressBar_Disgust->setMaximum(100);
    ui->progressBar_Disgust->setValue(0);
    ui->progressBar_Anger->setMaximum(100);
    ui->progressBar_Anger->setValue(0);
    ui->progressBar_Anticipation->setMaximum(100);
    ui->progressBar_Anticipation->setValue(0);
    ui->progressBar_Ecstasy->setMaximum(100);
    ui->progressBar_Ecstasy->setValue(0);
    ui->progressBar_Admiration->setMaximum(100);
    ui->progressBar_Admiration->setValue(0);
    ui->progressBar_Terror->setMaximum(100);
    ui->progressBar_Terror->setValue(0);
    ui->progressBar_Amazement->setMaximum(100);
    ui->progressBar_Amazement->setValue(0);
    ui->progressBar_Grief->setMaximum(100);
    ui->progressBar_Grief->setValue(0);
    ui->progressBar_Loathing->setMaximum(100);
    ui->progressBar_Loathing->setValue(0);
    ui->progressBar_Rage->setMaximum(100);
    ui->progressBar_Rage->setValue(0);
    ui->progressBar_Vigilance->setMaximum(100);
    ui->progressBar_Vigilance->setValue(0);
    /*                                       */
    ui->progressBar_Aggressiveness->setMaximum(100);
    ui->progressBar_Aggressiveness->setValue(0);
    ui->progressBar_Optimism->setMaximum(100);
    ui->progressBar_Optimism->setValue(0);
    ui->progressBar_Love->setMaximum(100);
    ui->progressBar_Love->setValue(0);
    ui->progressBar_Submission->setMaximum(100);
    ui->progressBar_Submission->setValue(0);
    ui->progressBar_Awe->setMaximum(100);
    ui->progressBar_Awe->setValue(0);
    ui->progressBar_Disapproval->setMaximum(100);
    ui->progressBar_Disapproval->setValue(0);
    ui->progressBar_Remorse->setMaximum(100);
    ui->progressBar_Remorse->setValue(0);
    ui->progressBar_Contempt->setMaximum(100);
    ui->progressBar_Contempt->setValue(0);
    ui->progressBar_Envy->setMaximum(100);
    ui->progressBar_Envy->setValue(0);
    ui->progressBar_Cynicism->setMaximum(100);
    ui->progressBar_Cynicism->setValue(0);
    ui->progressBar_Pride->setMaximum(100);
    ui->progressBar_Pride->setValue(0);
    ui->progressBar_Hope->setMaximum(100);
    ui->progressBar_Hope->setValue(0);
    ui->progressBar_Guilt->setMaximum(100);
    ui->progressBar_Guilt->setValue(0);
    ui->progressBar_Curiosity->setMaximum(100);
    ui->progressBar_Curiosity->setValue(0);
    ui->progressBar_Despair->setMaximum(100);
    ui->progressBar_Despair->setValue(0);
    ui->progressBar_Unbelief->setMaximum(100);
    ui->progressBar_Unbelief->setValue(0);
    ui->progressBar_Sentimentality->setMaximum(100);
    ui->progressBar_Sentimentality->setValue(0);
    ui->progressBar_Shame->setMaximum(100);
    ui->progressBar_Shame->setValue(0);
    ui->progressBar_Outrage->setMaximum(100);
    ui->progressBar_Outrage->setValue(0);
    ui->progressBar_Pessimism->setMaximum(100);
    ui->progressBar_Pessimism->setValue(0);
    ui->progressBar_Morbidness->setMaximum(100);
    ui->progressBar_Morbidness->setValue(0);
    ui->progressBar_Dominance->setMaximum(100);
    ui->progressBar_Dominance->setValue(0);
    ui->progressBar_Anxiety->setMaximum(100);
    ui->progressBar_Anxiety->setValue(0);
    ui->progressBar_Delight->setMaximum(100);
    ui->progressBar_Delight->setValue(0);
}

void Emotions::on_actionLoad_triggered()
{
    // open file code adapted from Qt getting started notepad example //
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QString());
    update_status_bar(fileName);

    if (!fileName.isEmpty()) {
        file_csv.setFileName(fileName);
        if (!file_csv.open(QIODevice::ReadWrite)) {
            QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
            return;
        }
        QTextStream in(&file_csv);
        /* format expected is a C.omma S.eparated V.alues one, like so :
         * 0, 2016/01/02
         * 5, 2016/02/01
         * 23, 2016/03/04
         *
         * dates in ISO format
         */
        short s;
        QChar c;
        QDateTime d;
        QString line;
        while(!in.atEnd()) {
            in >> s;
            if (s < 0 || s > 47) {
                // badly formatted input //
                QMessageBox::critical(this, tr("Error"), tr("Badly formatted input, expected number"));
                break;
            }
            in >> c;
            if (c != ',') {
                // badly formatted input //
                QMessageBox::critical(this, tr("Error"), tr("Badly formatted input, expected comma ','"));
                break;
            }
            line = in.readLine();
            d = QDateTime::fromString(line, Qt::ISODate);
            if (!d.isValid()) {
                // badly formatted input //
                QMessageBox::critical(this, tr("Error"), tr("Badly formatted input, expected iso 8601 date"));
                break;
            }
            // all is good, update emotion //
            emotion[s]++;
        }
        update_ratios();
//        file.close(); // closing in destructor //
    }
}
