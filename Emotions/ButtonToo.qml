import QtQuick 2.4

ButtonTooForm {
    Rectangle {
        id: rectangle
        color: "#ffffff"
        anchors.fill: parent
    }

    Text {
        id: text1
        text: qsTr("Text")
        anchors.fill: parent
        font.pixelSize: 12
    }
}
