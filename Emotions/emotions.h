#ifndef EMOTIONS_H
#define EMOTIONS_H

#include <QMainWindow>

#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

#include <QDateTime>

namespace Ui {
class Emotions;
}

class Emotions : public QMainWindow
{
    Q_OBJECT

public:
    explicit Emotions(QWidget *parent = 0);
    ~Emotions();

private slots:
    void on_pushButton_Serenity_pressed();

    void on_pushButton_Acceptance_pressed();

    void on_pushButton_Apprehension_pressed();

    void on_pushButton_Distraction_pressed();

    void on_pushButton_Pensiveness_pressed();

    void on_pushButton_Boredom_pressed();

    void on_pushButton_Annoyance_pressed();

    void on_pushButton_Interest_pressed();

    void on_pushButton_Joy_pressed();

    void on_pushButton_Trust_pressed();

    void on_pushButton_Fear_pressed();

    void on_pushButton_Surprise_pressed();

    void on_pushButton_Sadness_pressed();

    void on_pushButton_Disgust_pressed();

    void on_pushButton_Anger_pressed();

    void on_pushButton_Anticipation_pressed();

    void on_pushButton_Ecstasy_pressed();

    void on_pushButton_Admiration_pressed();

    void on_pushButton_Terror_pressed();

    void on_pushButton_Amazement_pressed();

    void on_pushButton_Grief_pressed();

    void on_pushButton_Loathing_pressed();

    void on_pushButton_Rage_pressed();

    void on_pushButton_Vigilance_pressed();

    /*
     * Dyads (combinations)
     * */

    void on_actionClear_triggered();

    void on_actionLoad_triggered();

    void on_pushButton_Aggressiveness_pressed();

    void on_pushButton_Optimism_pressed();

    void on_pushButton_Love_pressed();

    void on_pushButton_Submission_pressed();

    void on_pushButton_Awe_pressed();

    void on_pushButton_Disapproval_pressed();

    void on_pushButton_Remorse_pressed();

    void on_pushButton_Contempt_pressed();

    void on_pushButton_Envy_pressed();

    void on_pushButton_Cynicism_pressed();

    void on_pushButton_Pride_pressed();

    void on_pushButton_Hope_pressed();

    void on_pushButton_Guilt_pressed();

    void on_pushButton_Curiosity_pressed();

    void on_pushButton_Despair_pressed();

    void on_pushButton_Unbelief_pressed();

    void on_pushButton_Sentimentality_pressed();

    void on_pushButton_Shame_pressed();

    void on_pushButton_Outrage_pressed();

    void on_pushButton_Pessimism_pressed();

    void on_pushButton_Morbidness_pressed();

    void on_pushButton_Dominance_pressed();

    void on_pushButton_Anxiety_pressed();

    void on_pushButton_Delight_pressed();

private:
    QLabel *statusBarLabel;
    void pushButton_pressed(const uchar& i);
    short emotion_max(void) const;
    void emotions_clear(void);
    QFile file_csv;
    void append_emotion(const ushort& emotion_id);
    void update_ratios(void);
    void update_status_bar(const QString& fileName);
    Ui::Emotions *ui;
    int emotions_Total;
    short emotion[48];
};

#endif // EMOTIONS_H
