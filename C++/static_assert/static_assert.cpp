int main (void)
{
	constexpr int max_speed = 3e8;
	constexpr int my_current_speed = (1 << 30) - 1;
	static_assert(my_current_speed < max_speed, "would you picture that!?");
	return 0;
}
