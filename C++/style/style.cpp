#include <iostream>
using namespace std;

double square (double x)
{
	return x * x;
}

double cube (double x)
{
	return square(x) * x;
}

int main()
{
	cout << "cube of 7 is " << cube(7) << endl;
}
