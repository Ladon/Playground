#include <iostream>
using std::cout;
using std::endl;

int main()
{
	auto c = '4';

	auto i = 3;
	cout << sizeof(i) << endl;

	auto j = 18446744073709551615; // 2 ^ 64 - 1
	cout << sizeof(j) << endl;
}
