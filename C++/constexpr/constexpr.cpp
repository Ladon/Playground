#include <iostream>
using namespace std;

constexpr unsigned long long sum (unsigned short n)
{
	return (!n) ? 0 : n + sum(n - 1);
}

constexpr unsigned long long l = 10000000;

int main()
{
	constexpr unsigned long long s = sum(l);
}
