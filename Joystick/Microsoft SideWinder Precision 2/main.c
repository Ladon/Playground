#include <fcntl.h> // open...
#include <stdio.h> // printf...
#include <sys/types.h> // timeval...
#include <unistd.h> // read...

typedef signed char i8;
typedef unsigned char u8;
int main (int argc, char* argv[])
{
	///////////////////////
	//                   //
	// preliminary stuff //
	//                   //
	///////////////////////
	if (argc != 2) {
		printf ("argument missing. Usage ::\n%s /dev/input/js?\n",argv[0]);
		return -1;
	}
	printf("using %s. Is this correct (Y/N) : ",argv[1]);
	i8 ans = -1;
	do {
		int c = getchar();
		switch (c) {
		case 'Y': case 'y':
			ans = 1;
			break;
		case 'N': case 'n':
			ans = 0;
			break;
		}
	} while (ans == -1);
	if (ans == 0)
		return 1;
	//////////////////////
	//                  //
	// start of program //
	//                  //
	//////////////////////
	struct input_event {
		struct timeval time;
		unsigned short type; // https://www.kernel.org/doc/html/v4.12/input/event-codes.html#input-event-codes //
		unsigned short code;
		unsigned int value;
	}buf; // https://www.kernel.org/doc/html/v4.12/input/input.html?highlight=evdev#event-interface //
	int fd = open (argv[1], O_RDONLY);
	if (fd < 0) { // FIXME {fd <= 0}?
		printf("Sorry but I could not open \"%s\". Aborting...\n",argv[1]);
		return fd;
	}
	int bytes_read = read(fd, &buf, sizeof(buf));
	if (bytes_read < 0) { // 0 on EOF //
		printf("Sorry but I could not read anything from \"%s\".\n", argv[1]);
		return bytes_read;
	}
	printf("type \t: %#4hx\n", buf.type);
	printf("code \t: %#4hx\n", buf.code);
	printf("value \t: %#8x\n", buf.value);
	return 0;
}
