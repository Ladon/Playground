#include <chrono>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
// #include <ifstream>
#include <ncurses.h>
#include <thread>

using std::min;

using std::literals::chrono_literals::operator ""ms;
using std::this_thread::sleep_for;

/*
	12bit accelerometer lis3lv02d on some hp laptops, at least
	z points down
	x points left
	y points in

	TODO : 
		last * print Gs (note : read kernel doc about 1/1000G = mg)
			   (note : use sqrt(x^2 + y^2 + z^2))
		last * learn how to read properly
		2nd * set X when in, o in head when out
		1st * simplify code to be easily updated
		
*/

int main(void)
{
	// start //
	initscr();
	int rows, cols;
	getmaxyx(stdscr, rows, cols);
	float center_x, center_y;
	center_x = cols / 2.0;
	center_y = rows / 2.0;
	curs_set(0);
	noecho();
	char filename[] = "/sys/devices/platform/lis3lv02d/position";
	FILE* accel;

	// middle //
	int x, y, z;
	x = 0;
	y = 0;
	z = 0;
	// format --> (num1,num2,num3) <--
	int tmp = 0;
	while (true) {
		accel = fopen(filename,"r");
		if (accel == NULL) {
			mvaddstr(rows - 1, 0, "ERROR : could not open file");
			addstr(filename);
			addstr(" ! hit any key to exit");
			refresh();
			getch();
			endwin();
			return 1;
		}
		tmp = fscanf(accel, "(%d,%d,%d)", &x, &y, &z);
		if (tmp == EOF) {
			fclose(accel);
			mvaddstr(rows - 1, 0, "ERROR : problem reading data");
			refresh();
			getch();
			endwin();
			return 2;
		}
//		rewind(accel);
		fclose(accel);

		// play //
		float amp = sqrt(x * x + y * y + z * z);
		// scale //
		float R = min(cols / 2.0, rows / 2.0);
		float k = -x / amp;
		float j = z / amp;
		float r = amp / 4095.0 * R;
		for (float dis = 0; dis <= r; dis += 0.1) {
			mvaddch(center_y + j * dis, center_x + k * dis, '*');
		}
		// display //
		refresh();
		sleep_for(700ms);
		// clear //
		for (float dis = 0; dis <= r; dis += 0.1) {
			mvaddch(center_y + j * dis, center_x + k * dis, ' ');
		}
//		refresh();

		// old code //
//		char buff[80] = {0};
//		sprintf(buff, "%d/%d/%d", x, y, z);
//		char empty[] = "                       ";
//		mvaddstr(rows / 2, cols / 2 - strlen(empty) / 2, empty);
//		mvaddstr(rows / 2, cols / 2 - strlen(buff) / 2, buff);
//		refresh();
//		sleep_for(1300ms);
		//         //
	}

	// end //
	mvgetch(rows - 1,0);
	endwin();
	return 0;
}
