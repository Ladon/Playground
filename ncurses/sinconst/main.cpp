#include <cmath>
#include <ncurses.h>

int main(void)
{
	// start //
	initscr();
	int rows, cols;
	getmaxyx(stdscr, rows, cols);
	char* hor = new char[cols + 1]; // horizontal line

	// x axis //
	for (int i = 0; i < cols; i++)
		hor[i] = '-';
	hor[cols] = '\0';

	// process //
	constexpr float pi = 3.141593;
	float A = rows / 4.0;
	float fin = 5.0 * pi;
	float fi = 0;
	for (int i = 0; i < cols; i++) {
		fi = A * sin(fin * i / (cols - 1.0));
		mvaddch(rows / 2.0 + fi, i, '*'); // FIXME : check for cols > 1 //
	}

	// x axis //
	mvprintw(rows / 2, 0, hor);
	delete[] hor;

	// y axis //
	for (int i = 0; i < rows; i++)
		mvaddch(i, cols / 2,'|');

	// end //
	refresh();

	getch();
	endwin();
	return 0;
}
