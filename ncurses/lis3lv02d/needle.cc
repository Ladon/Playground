#include "needle.h"

// ...structors //
Needle::Needle (void) : angle(M_PI_2), radius(0), radius_max(0)  
{
}
//////////////////
void Needle::set_angle (const float& angle_arg)
{
	// trim angle to 0..360 //
	constexpr float two_pi = 2.0 * M_PI;
	this->angle = angle_arg;
	while (this->angle < 0)
		this->angle += two_pi;
	while (this->angle > two_pi)
		this->angle -= two_pi;
}

void Needle::set_needle (const float& angle_arg, const float& radius_arg)
{
	this->set_angle(angle_arg);
	this->set_radius(radius_arg);
}

// PRECONDITION :                             //
// radius is less than or equal to radius_max //
// radius is non-negative                     //
void Needle::set_radius (const float& radius_arg)
{
	#ifdef ASSERTS
	assert (radius_arg >= 0);
	assert (radius_arg <= this->radius_max);
	#endif
	this->radius = radius_arg;
}

void Needle::set_radius_max (const float& radius_max_arg)
{
	#ifdef ASSERTS
	assert (radius_max_arg > 0);
	#endif
	this->radius_max = radius_max_arg;
}
