#pragma once
#include <cmath>

#ifdef ASSERTS
#include <cassert>
#endif

class Needle {
public :
	// ...structors //
	Needle (void);
	void draw_frame (void);
	void draw_needle (void);
	void erase_needle (void);
	// PRECONDITION : radius is less than or equal to radius_max //
	void set_angle (const float& angle);
	void set_needle (const float& angle, const float& radius);
	void set_radius (const float& radius);
	void set_radius_max (const float& radius_max);
private :
	float angle;
	float radius;
	float radius_max;
};
