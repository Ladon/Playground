#include <chrono>
#include <cmath>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
// #include <experimental/random> // randint(mininc,maxinc)
// #include <ifstream>
#include <ncurses.h>
#include <thread>

#include "needle.h"

using std::max;
using std::min;

using std::literals::chrono_literals::operator ""ms;
using std::this_thread::sleep_for;

// using std::experimental::randint;
/*
	12bit accelerometer lis3lv02d on some hp laptops, at least
	z points down
	x points left
	y points in

	DOING :
		* set color to blue is min, red is max
	TODO :
		* simplify code to be easily updated
		* learn how to read properly
		* print Gs (note : read kernel doc about 1/1000G = mg)
			   (note : use sqrt(x^2 + y^2 + z^2))
		* set needle to automax
		* test if "peek at file" works (in order to avoid close/re-open)
		* make delay single one (at the end of one loop)
		* make cummulative graph, inside circle, opposite radial, gray
	DONE :
		* form second circle to the right with variable-R circle for yy'
		* set X when in, o in head when out
		* normalize in circle
*/

/* sample code to print to center of screen 
//		char buff[80] = {0};
//		sprintf(buff, "%d/%d/%d", x, y, z);
//		char empty[] = "                       ";
//		mvaddstr(rows / 2, cols / 2 - strlen(empty) / 2, empty);
//		mvaddstr(rows / 2, cols / 2 - strlen(buff) / 2, buff);
//		refresh();
//		sleep_for(1300ms);
*/

// display rows x columns //
int rows, cols; // updated by main at beginning //
// PRECONDITION : format length <= 80 // 
void center_print (const char* format, ...) // copied __sprintf() //
{
	va_list arg;
	va_start(arg, format);
	char buff[81];
	vsprintf(buff, format, arg);
	va_end(arg);
	// clear line in order to print //
	char empty[81];
	int buff_l;
	buff_l = strlen(buff);
	for (char i = 0; i < 80; i++)
		empty[i] = ' ';
	empty[80] = '\0';
	// move to center and print //
	mvaddstr(rows / 2, cols / 2 - 80 / 2, empty);
	mvaddstr(rows / 2, cols / 2 - buff_l / 2, buff);
}

void clear_circle(int rows, int cols, float aty, float atx, float R);
short color_at_in(float at, float in);
void draw_circle(int rows, int cols, float aty, float atx, float R);
void draw_circle(int rows, int cols, float aty, float atx, float R, char c);
FILE* open_file(char* filename);
int read_file(FILE* accel, int& x, int& y, int& z);

class Screen {
public:
	Screen (void)
	{
		this->initialize();
	}
	~Screen (void)
	{
		endwin();
	}
	int get_cols (void);
	float get_center_x (void);
	float get_center_y (void);
	int get_rows (void);
private:
	inline void initialize(void)
	{
		initscr();
		curs_set(0);
		noecho();
		start_color();
		getmaxyx(stdscr, this->rows, this->cols);
	}
	float mid_x, mid_y;
	int cols,rows;
};

int main(void)
{
	// initialize //
	initscr();
	curs_set(0);
	noecho();
	start_color();
	// get rows & columns //
	getmaxyx(stdscr, rows, cols);
	// find center //
	float center_x, center_y;
	float needle_x, needle_y;
	float circle_x, circle_y;
	center_x = cols / 2.0;
	center_y = rows / 2.0;
	needle_x = cols / 4.0;
	needle_y = center_y;
	circle_x = 3 * cols / 4.0;
	circle_y = center_y;
	// set filename //
	char filename[] = "/sys/devices/platform/lis3lv02d/position";
	FILE* accel;
	// classes //
	Needle needle;

	// some variables //
	char sum_l;
	sum_l = 11;  // 11 parts of the circle //
	float sum[sum_l] = {0};
	// test draw START //
	for (int i = 0; i < sum_l; i++) {
		sum[i] = (i + 1) * 100;
	}
	int sum_max = (sum_l - 1 + 1) * 100; // scale radius to sum_max //
	// use needle_x, needle_y //
	int detail = 10; // max 10 parts per radius //
	float angle[sum_l]; // increasing | PRECONDITION //
	float angle_part = 2.0 * M_PI / sum_l;
	for (int i = 0; i < sum_l; i++) {
		// calculate the angle of each part //
		angle[i] = i * angle_part;
	}
	float r_max = min(cols / 2.0, rows / 2.0);
	for (int i = 0; i < sum_l; i++) {
		// draw circle parts //
		float r;
		r = sum[i] / sum_max * r_max; // scaling of part to circle //
		int x, y; // tmp //
		for (float j = r; j <= r_max; j += 1) {
			// draw single part //
			for (float k = angle[i]; k < angle[i] + angle_part; k += 0.01) {
				x = needle_x + j * cos(k);
				y = needle_y + j * sin(k);
				mvaddch (y, x, '&');
			}
		}
	}
	
//	mvgetch(rows - 1,0);
//	endwin();
//	return 0;
	// test draw END //

	// draw circle //
	{
		float R = min(rows / 2.0, cols / 2.0) + 1;
		draw_circle(rows, cols, needle_y, needle_x, R);
	}
	// get x, y, z //
	int x, y, z;
	x = 0;
	y = 0;
	z = 0;
	int max_y;
	max_y = 0;
	// format is "(num1,num2,num3)"
	int tmp = 0; // read return code //
	while (true) {
		/* * * * * * * *
		 *             *
		 *   F I L E   *
		 *             *
		 * * * * * * * */
		// try to open file //
		accel = open_file(filename);
		// check if opened //
		if (accel == NULL)
			return 1;
		// try to read file //
		tmp = read_file(accel, x, y, z);
	        // check if read //
		if (tmp == EOF)
			return 2;
		fclose(accel); // close //
		/* * * * * * * *
		 *             *
		 *   Ǝ ꓶ I ꓞ   *
		 *             *
		 * * * * * * * */

		/* * * * * * * * * *
		 *                 *
		 *   N E E D L E   *
		 *                 *
		 * * * * * * * * * */
		// amplitude //
		float amp = sqrt(x * x + z * z);
		// R is max radius of the display //
		float R = min(cols / 2.0, rows / 2.0);
		const float sup_sense = 4095; // 12bit of precision -> 2 ^ 12 - 1
		// normalize //
		float k = -x / amp; // reverse yy'
		float j = z / amp; // xx'
		// max print radius //
                float r;
                // OPTION - scale max to window //
//	        r = amp / sup_sense * R;
		// OPTION - fill display //
		r = R;
		short max_steps;
		max_steps = R;
		short color_pair;
		color_pair = 0;
		float step;
		// R max steps or R *pixels* //
		step = 1 / 3.0; // simulate continuous needle //
		// dis.tance from center //
		// R is possible max value //
		// r is current max value //
		for (float dis = 0; dis <= r; dis += step) {
			/* colors will go :
			 * blue for center,
			 * red for max
			 * green for middle
			 * 
			 *      C                             Mid                Max
			 *    1000B                          1000G              1000R
			 * C -> Mid : (0,(1-abs(max/2-amp))/(max/2)*1000,(max/2 - amp)/(max/2)*1000)
			 * Mid -> Max : (1000*(amp - max/2)/(max/2),(max/2-(amp-max/2))/(max/2)*1000,0)
			 */

			/* I SUPPOSE colors go all the way to 255
			 * so i'll use 9..255
			 * NEED to scale needle to 246
			 * I CHOOSE to scale down to 128 and use 128..255
			 */
//			color_pair = color_at_in(dis / r * amp, amp);
			color_pair = color_at_in(dis / r * amp, sup_sense);

			attron(COLOR_PAIR(color_pair));
			mvaddch(needle_y + j * dis, needle_x + k * dis, '*');
			attroff(COLOR_PAIR(color_pair));
		}
		// point inwards or outwards //
		if (y > 0) {
			mvaddch(needle_y +j * r, needle_x + k * r, 'o');
		} else if (y < 0) {
			mvaddch(needle_y, needle_x, 'X');
		}
		/* * * * * * * * * *
		 *                 *
		 *   Ǝ ꓶ ꓷ Ǝ Ǝ N   *
		 *                 *
		 * * * * * * * * * */

		/* * * * * * * * * * * * * * * * * * * * * * *
		 *                                           *
		 *   I N  W A R D S   /   O U T  W A R D S   *
		 *                                           *
		 * * * * * * * * * * * * * * * * * * * * * * */
		max_y = max(max_y, abs(y));
		float circle_r;
		if (max_y != 0)
			circle_r = abs(y) / (float) max_y * R;
		else
			circle_r = abs(y) / (float) sup_sense * R;
		// sign //
		if (y > 0)
			mvaddch(circle_y, circle_x, 'O');
		else if (y < 0)
			mvaddch(circle_y, circle_x, 'X');
		draw_circle(rows, cols, circle_y, circle_x, circle_r, '*');
		/* * * * * * * * * * * * * * * * * * * * * * *
		 *                                           *
		 *   S ꓷ ꓤ ꓯ M  ꓕ ꓵ O   /   S ꓷ ꓤ ꓯ M  N I   *
		 *                                           *
		 * * * * * * * * * * * * * * * * * * * * * * */
		// display //
		refresh();
		sleep_for(400ms); // do NOT add more delays, keep single //
		// clear //
		for (float dis = 0; dis <= r; dis += 0.1) {
			mvaddch(needle_y + j * dis, needle_x + k * dis, ' ');
		}
		clear_circle(rows, cols, circle_y, circle_x, circle_r);
		// clear circle sign //
		mvaddch(circle_y, circle_x, ' '); // can do without //
	}

	// end //
	mvgetch(rows - 1,0);
	endwin();
	return 0;
}

// let's hav sum fun //
void clear_circle(int rows, int cols, float aty, float atx, float R)
{
	draw_circle(rows, cols, aty, atx, R, ' ');
}

// will use colors and pairs in range 64..127 //
// PRECONDITION
//     at >= 0
//     in >= 2
//     at <= in
short color_at_in(float at, float in)
{
	float half;
	half = in / 2.0;
	// colors in 0..1000 //
	float red, green, blue;
	red = 0;
	green = 0;
	blue = 0;
	if (at < half) {
		red = 0;
		green = at / half;
		blue = (half - at) / half;

		green *= 1000;
		blue *= 1000;
	} else {
		red = (at - half) / half;
		green = (half - (at - half)) / half;
		blue = 0;

		red *= 1000;
		green *= 1000;
	}
	float ratio;
	ratio = at / in;
	short color;
	color = 64 + 63 * ratio; // CHECK this //
	short pair;
	pair = color;

	init_color(color, red, green, blue);
	init_pair(pair, color, COLOR_BLACK);
	return pair;
}

void draw_circle(int rows, int cols, float aty, float atx, float R)
{
	draw_circle(rows, cols, aty, atx, R, '#');
}

void draw_circle(int rows, int cols, float aty, float atx, float R, char c)
{
	const float PI = 3.141593;
	// do not overlap //
//	float R = min(rows / 2.0, cols / 2.0) + 1;
	float max_i = 4 * R;
	const float full_circle = 2 * PI;
	for (float i = 0; i <= max_i; i += 1) {
		// scale i to angle //
		float angle = i / max_i * full_circle;
		float y, x;
		y = aty + R * sin(angle);
		x = atx + R * cos(angle);
		if (y < rows && y >= 0 && x < cols && x >= 0)
			mvaddch(y, x, c);
	}
}

FILE* open_file(char* filename)
{
	FILE* accel = fopen(filename,"r"); // open readonly //
	// check if opened //
	if (accel == NULL) {
		center_print("ERROR : could not open file %s! hit any key to exit", filename);
		refresh();
		getch();
		endwin();
	}
	return accel;
}

int read_file(FILE* accel, int& x, int& y, int& z)
{
		int tmp = fscanf(accel, "(%d,%d,%d)", &x, &y, &z);
	        // check if read //
		if (tmp == EOF) {
			fclose(accel);
			center_print("ERROR : problem reading data");
			refresh();
			getch();
			endwin();
		}
		return tmp;
}

