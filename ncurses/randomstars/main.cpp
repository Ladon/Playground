#include <chrono>
#include <thread>
// #include <cmath> // sin()
#include <cstdlib> // rand()
#include <ctime> // time(nullptr)
#include <ncurses.h>

// using namespace std; // FIXME : limit to sleep_for(100ms)
// DONE
using std::literals::chrono_literals::operator ""ms;

int main(void)
{
	// start //
	initscr();
	int rows, cols;
	getmaxyx(stdscr, rows, cols);

	srand(time(nullptr));
	for (int i = 0; i < cols*rows; i++) {
		mvaddch(rand()/(float) RAND_MAX * rows, rand()/(float) RAND_MAX * cols, '*');
		refresh();
		std::this_thread::sleep_for(10ms);
	}

	// end //

	mvgetch(rows - 1,0);
	endwin();
	return 0;
}
