#pragma once

template <class T> struct llist {
	T i;
	struct llist<T>* next;
	struct llist<T>* prev;
};

template <class T> class stack {
private :
	struct llist<T>* ll;
public :
        stack() : ll(NULL) {}
	// PRECONDITION
	~stack();
	// POSTCONDITION
	// list is null

        // PRECONDITION
	llist<T>* push(const T& i);
	// POSTCONDITION
	// list contains i at end

	// PRECONDITION
	// list has items
	T pop(void);
	// POSTCONDITION
	// NULL if no items
	// else the top item
};

using std::cerr;
using std::endl;

template <class T> stack<T>::~stack()
{
	if (ll != NULL) {
		llist<T>* lltmp;
		while (ll->next != NULL) {
			lltmp = ll;
			ll = ll->next;
			delete lltmp;
		}
		delete ll;
	}
}

template <class T> llist<T>* stack<T>::push(const T& i)
{
	llist<T>* lltmp = ll;
	if (lltmp == NULL) { // list is empty //
		lltmp = new llist<T>;
		// check for bad allocation //
		if (lltmp == NULL) {
			cerr << "EE: Allocation of memory failed. Requested "
			     << "size was : " << sizeof(llist<T>) 
                             << ". @stack::push(const int& i)" << endl;
			return NULL;
		}
		// allocation was successfull //
		lltmp->prev = lltmp->next = NULL;
		lltmp->i = i;
		// set start to new memory //
		ll = lltmp;
	} else {
		// reach last item //
		while (lltmp->next != NULL)
			lltmp = lltmp->next;
		lltmp->next = new llist<T>;
		// check for bad allocation //
		if (lltmp->next == NULL) {
			cerr << "EE: Allocation of memory failed. Requested "
                             << "size was : " << sizeof(llist<T>)
			     << ". @stack::push(const int& i)" << endl;
			return NULL;
		}
		// allocation was successfull //
		lltmp->next->prev = lltmp;
		lltmp->next->next = NULL;
		lltmp->next->i = i;
		lltmp = lltmp->next;
	}
	return lltmp;
}

template <class T> T stack<T>::pop(void)
{
	if (ll == NULL) {
		cerr << "WW: Trying to pop empty stack!" << endl;
		return -1;
	}
	// find last //
	llist<T>* lltmp = ll;
	while (lltmp->next != NULL)
		lltmp = lltmp->next;
	// remove last
	// update previous
	if (lltmp == ll) // single item //
		ll = NULL;
	else
		lltmp->prev->next = NULL;
	T i = lltmp->i;
	delete lltmp;
	return i;
}
// as advised by //
// https://stackoverflow.com/questions/ //
// 8752837/undefined-reference-to-template-class-constructor#8752879 //
/* template class stack<char>;
 * template class stack<short>;
 * template class stack<int>;
 * template class stack<long>;
 * template class stack<float>;
 * template class stack<double>; */
// or include the code of the template into the header (as is customary) //
