#!/bin/bash
clear \
	&& make clean \
	&& make \
	&& valgrind --leak-check=full --show-leak-kinds=all ./main
