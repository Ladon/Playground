#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;

void delay (clock_t d)
{
	clock_t c = clock();
	while(clock() - c <= d);
}

// /sys/devices/platform/lis3lv02d/position // (##,##,##)
int main ()
{
	ifstream pos("/sys/devices/platform/lis3lv02d/position");
	int x,y,z;
	char c;
	cin >> c >> x >> c >> y >> c >> z >> c;
	cout << x << ", " << y << ", " << z << endl;
	char roller[] = "|/-\\"; // use Gentoo, be ecstatic
	while (true) {
		for (auto i : roller) {
			cout << i;
			delay(100);
			cout << "\b";
		}
	}
}
