#include <iostream>
#include <cmath>
#include <chrono>
#include <thread>
using namespace std;

typedef unsigned char u8;
int main(void)
{
	u8 lsize = 81;
	char line[lsize];
	// initialize line //
	for (u8 i = 0; i < lsize - 1; i++) {
		line[i] = ' '; // 0x20
	}
	line[lsize - 1] = '\n';
	// sine //
	u8 center = lsize / 2;
	u8 pos = center;
	line[pos] = '*';
	u8 amp = 10;
	u8 period = 30;
	constexpr double pi = 3.141593;
	u8 cycles = 100;
	u8 time = 0;
	while (cycles) {
		line[pos] = 0x20;
		pos = center + amp * sin(2.0 * pi / period * time);
		line[pos] = '*';
		cout << line;
		// //
		time++;
		if (time >= period) {
			time = 0;
			cycles--;
		}
		this_thread::sleep_for(100ms);
	}
	return 0;
}
