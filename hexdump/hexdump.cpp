#include "hexdump.h"
#include "colorize.h"

/* DESCRIPTION :
 * a variable to transfer colorized output to main()
 */
extern char buff[];

/* DESCRIPTION :
 * the symbols to be used with each hexadecimal number
 */
char symbols[16] = {
	' ', '!', '@', '#', '$', '%', '^', '&',
	'*', '(', '~', '.', ';', '\'', '-', '|'};

/* DESCRIPTION :
 * visualize a file's binary code with colorful symbols
 */
int main (int argc, char* argv[])
{
	/* * * * * * * * * * * *
	 *                     *
	 *     print usage     *
	 *                     *
	 * * * * * * * * * * * */
	if (argc != 2) {
		cerr << "Usage : \n\t" <<
			__FILE__ << " filetodump" << endl;
		return -2;
	}
	/* * * * * * * * * * * *
	 *                     *
	 *      open file      *
	 *                     *
	 * * * * * * * * * * * */
	FILE* fp;
	fp = fopen(argv[1],"rb");
	if (fp == NULL) {
		cerr 	<< "EE : could not open " << argv[1]
			<< " for reading! Exiting..." << endl;
		return -1;
	}
	/* * * * * * * * * * * *
	 *                     *
	 *      read file      *
	 *                     *
	 * * * * * * * * * * * */
	unsigned char c; // one byte at a time //
	short size_c = sizeof(c);
	fread(&c,size_c,1,fp);
	short column = 0; // index for newline //
	short line = 0; // index for line info //
	while (!feof(fp)) {
		/* * * * * * * * * * * * * * * *
		 *                             *
		 *      convert and print      *
		 *                             *
		 * * * * * * * * * * * * * * * */
		unsigned char msnib = c >> 4;
		unsigned char lsnib = c & 0xF;
		// FIXME : the following two colorize()s use the same //
		// buff[] 					      //
		printf("%s",colorize(symbols[msnib], msnib));
		printf("%s",colorize(symbols[lsnib], lsnib));
//		printf("%c%c", symbols[msnib], symbols[lsnib]);
		column += 2;
		/* * * * * * * * * * * * * *
		 *                         *
		 *      print newline      *
		 *                         *
		 * * * * * * * * * * * * * */
		if (column >= 80) {
			cout << endl;
			column = 0;
			line++;
		}
		fread(&c,size_c,1,fp); // next byte //
	}
	if(column != 0)
		cout << endl;
	/* * * * * * * * * * *
	 *                   *
	 *    close file     *
	 *                   *
	 * * * * * * * * * * */
	fclose(fp);
	return 0;
}
