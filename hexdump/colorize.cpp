#include "colorize.h"

/* DESCRIPTION :
 * a variable to transfer colorized output to main()
 * */
char buff[19];

/* PRECONDITION :
 * paint between 0 and F inclusive
 * c is a printable character
 */
const char* colorize(const unsigned char& c, unsigned char paint)
{
	// abstain from errors //
	paint &= 0xF;
	paint += '0';
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * \x1B[3#m is a color                                   *
	 * \x1B[1m  is bright                                    *
	 * \x1B[39m is normal color                              *
	 * \x1B[21m is reset bright                              *
	 * \x1B[0m  is reset all (in case \x1B[39m does not work *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	if (paint <= '7') {
		/* * * * * * * * * * * * *
		 *                       *
		 *      normal color     *
		 *                       *
		 * * * * * * * * * * * * */
		sprintf(buff, "\x1B[3%cm%c\x1B[39m", paint, c);
	} else {
		/* * * * * * * * * * * *
		 *                     *
		 *      set bright     *
		 *                     *
		 * * * * * * * * * * * */
		sprintf(buff, "\x1B[3%c;1m%c\x1B[39;21m", paint - 8, c);
	}
	return buff;
}
/* POSTCONDITION : 
 * buff contains colorized c for linux command line 
 */
