#pragma once

#include "hexdump.h"

// 16 colors		//
// 0 .. 7 are   NORMAL  //
// 8 .. 0xF are BRIGHT  //
/* see colorize.cpp for info
 */
const char* colorize(const unsigned char& c, unsigned char paint);
