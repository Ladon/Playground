#pragma once

#include <iostream>
// print on screen //

#include <cstdio>
// file input-output //
// printf //

using std::cerr;
using std::cout;
using std::endl;
using std::hex;
